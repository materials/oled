#!/bin/bash

for dir in X Y Z ; do
	echo "Mobility in $dir direction"
	cd $dir
	for temp in 2000 2500 3000 3500 4000 4500 5000 10000 20000 30000 40000 50000;do
		mu=$(grep '<mu' $temp/Out.KMC)
		echo $temp $mu
	done
	cd ../
	echo ""
done
