
import numpy as np

#-----molecule specification-----

nmol = 3000

molrange = range(1,nmol+1)
mol = []
for i in molrange:
    mol.append('mCP')

state_n = [0] * nmol
state_h = [+1] * nmol
state_e = [-1] * nmol

#-----energies-----

data = np.loadtxt("energy_split.out",unpack=True)

E_n_tot = data[0]
E_h_tot = data[1]
E_e_tot = data[2]
#x = data[9]
#y = data[10]
#z = data[11]

N = len(E_n_tot)
R = range(N)

E_neut = [ E_n_tot[i] - E_n_tot[i]
        for i in R]

E_hole = [ E_h_tot[i] - E_n_tot[i]
        for i in R]

E_elec = [ E_e_tot[i] - E_n_tot[i]
        for i in R]

#-----number of iterations and coordinates-----

tempdata = np.loadtxt("tempdata",unpack=True)

iter_n = tempdata[0]
iter_h = tempdata[1]
iter_e = tempdata[2]

x = tempdata[3]
y = tempdata[4]
z = tempdata[5]

char = []
for i in molrange:
    char.append('XXX 000')

#-----zip and output-----

out = ''
for i,j,k,En,l,Ee,m,Eh,n,iN,e,iE,h,iH,ch,x,y,z in zip(molrange,mol,state_n,E_neut,state_e,E_elec,state_h,E_hole,state_n,iter_n,state_e,iter_e,state_h,iter_h,char,x,y,z):
    out += '{:4d}{:>11}{:3d}{:>10.5f}{:4d}{:>10.5f}{:3d}{:>10.5f}{:3d}{:>6.1f}{:4d}{:>6.1f}{:3d}{:>6.1f}{:>9}{:>10.5f}{:>10.5f}{:>10.5f}\n'.format(i,j,k,En,l,Ee,m,Eh,n,iN,e,iE,h,iH,ch,x,y,z)

with open('Energy_HoleElectron.out', "w+") as f:
    f.write(out)
