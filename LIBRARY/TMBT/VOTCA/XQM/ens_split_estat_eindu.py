
import numpy as np

#-----read absolute energies-----

data = np.loadtxt("energy_split.out",unpack=True)

E_n_tot = data[0]
E_h_tot = data[1]
E_e_tot = data[2]

E_n_elec = data[3]
E_h_elec = data[4]
E_e_elec = data[5]

E_n_indu = data[6]
E_h_indu = data[7]
E_e_indu = data[8]

#-----energy difference-----

N = len(E_n_tot)
R = range(N)

delE_tot_h = [ E_n_tot[i] - E_h_tot[i]
        for i in R ]

delE_tot_e = [ E_n_tot[i] - E_e_tot[i]
        for i in R ]

delE_elec_h = [ E_n_elec[i] - E_h_elec[i]
        for i in R ]

delE_elec_e = [ E_n_elec[i] - E_e_elec[i]
        for i in R ]

delE_indu_h = [ E_n_indu[i] - E_h_indu[i]
        for i in R ]

delE_indu_e = [ E_n_indu[i] - E_e_indu[i]
        for i in R ]

#-----append to output-----

output = ''
for i,j,k,l,m,n in zip(delE_tot_h,delE_elec_h,delE_indu_h,delE_tot_e,delE_elec_e,delE_indu_e):
#for i,j,k in zip(delE_tot_h,delE_elec_h,delE_indu_h):
    output += '{:>10.5f}{:>10.5f}{:>10.5f}{:>10.5f}{:>10.5f}{:>10.5f}\n'.format(i,j,k,l,m,n)
    #output += '{:>10.5f}{:>10.5f}{:>10.5f}\n'.format(i,j,k)

with open('energy_contrib_elec_indu.out','w+') as f:
    f.write(output)

#-----mean values-----

print("Mean of total contribution for holes = ",np.mean(delE_tot_h))
print("Mean electrostatic contribution for holes = ",np.mean(delE_elec_h))
print("Mean induction contribution for holes",np.mean(delE_indu_h))
print("")
print("Mean of total contribution for electrons = ",np.mean(delE_tot_e))
print("Mean electrostatic contribution for electrons = ",np.mean(delE_elec_e))
print("Mean induction contribution for electrons",np.mean(delE_indu_e))
print("")

#-----deviation (sigma)-----

stddev_h = np.sqrt(2.0)*(np.var(delE_tot_h)/2.0)**0.5
stddev_e = np.sqrt(2.0)*(np.var(delE_tot_e)/2.0)**0.5

print("Standard deviation, holes = ",stddev_h)
print("Standard deviation, electrons = ",stddev_e)
