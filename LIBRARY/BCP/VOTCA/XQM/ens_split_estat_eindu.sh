#!/bin/bash

#----------Change number of molecules and name of the molecule----------

rm energy_split.out

for i in {1..3000}; do
	E_n_tot=$(grep $i:BCP:n xqm.jobs -A 3 | grep 'total unit' | head -1 | sed 's|<total unit="eV">||' | sed 's|</total>||' | awk '{print $1}')
	E_h_tot=$(grep $i:BCP:h xqm.jobs -A 3 | grep 'total unit' | head -1 | sed 's|<total unit="eV">||' | sed 's|</total>||' | awk '{print $1}')
	E_e_tot=$(grep $i:BCP:e xqm.jobs -A 3 | grep 'total unit' | head -1 | sed 's|<total unit="eV">||' | sed 's|</total>||' | awk '{print $1}')

	E_n_elec=$(grep $i:BCP:n xqm.jobs -A 4 | grep 'estat unit' | head -1 | sed 's|<estat unit="eV">||' | sed 's|</estat>||' | awk '{print $1}')
	E_h_elec=$(grep $i:BCP:h xqm.jobs -A 4 | grep 'estat unit' | head -1 | sed 's|<estat unit="eV">||' | sed 's|</estat>||' | awk '{print $1}')
	E_e_elec=$(grep $i:BCP:e xqm.jobs -A 4 | grep 'estat unit' | head -1 | sed 's|<estat unit="eV">||' | sed 's|</estat>||' | awk '{print $1}')

	E_n_indu=$(grep $i:BCP:n xqm.jobs -A 5 | grep 'eindu unit' | head -1 | sed 's|<eindu unit="eV">||' | sed 's|</eindu>||' | awk '{print $1}')
	E_h_indu=$(grep $i:BCP:h xqm.jobs -A 5 | grep 'eindu unit' | head -1 | sed 's|<eindu unit="eV">||' | sed 's|</eindu>||' | awk '{print $1}')
	E_e_indu=$(grep $i:BCP:e xqm.jobs -A 5 | grep 'eindu unit' | head -1 | sed 's|<eindu unit="eV">||' | sed 's|</eindu>||' | awk '{print $1}')


	echo $E_n_tot $E_h_tot $E_e_tot $E_n_elec $E_h_elec $E_e_elec $E_n_indu $E_h_indu $E_e_indu >> energy_split.out
done
