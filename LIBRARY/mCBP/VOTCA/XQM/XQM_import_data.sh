#!/bin/bash

#-----Changes need to be made before execution of this script-----
#Number of molecules
#Molecule name(s)

for i in {1..3000}; do
    Iter_N=$(grep $i:mCBP:n xqm.jobs -A 19 | grep iter | head -1 | sed 's|<iter>||' | sed 's|</iter>||' | awk '{print $1}')
    Iter_H=$(grep $i:mCBP:h xqm.jobs -A 19 | grep iter | head -1 | sed 's|<iter>||' | sed 's|</iter>||' | awk '{print $1}')
    Iter_E=$(grep $i:mCBP:e xqm.jobs -A 19 | grep iter | head -1 | sed 's|<iter>||' | sed 's|</iter>||' | awk '{print $1}')

    x=$(grep $i:mCBP:n xqm.jobs -A 2 | grep xyz | head -1 | sed 's|<xyz unit="nm">||' | sed 's|</xyz>||' | awk '{print $1}')
    y=$(grep $i:mCBP:n xqm.jobs -A 2 | grep xyz | head -1 | sed 's|<xyz unit="nm">||' | sed 's|</xyz>||' | awk '{print $2}')
    z=$(grep $i:mCBP:n xqm.jobs -A 2 | grep xyz | head -1 | sed 's|<xyz unit="nm">||' | sed 's|</xyz>||' | awk '{print $3}')

    echo $Iter_N $Iter_H $Iter_E $x $y $z >> tempdata
done

python XQM_import_data.py

rm tempdata
