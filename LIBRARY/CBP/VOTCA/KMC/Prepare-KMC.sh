#!/bin/bash

for dir in X Y Z;do
	mkdir $dir
	cd $dir

	rm Run-KMC.sh
	echo "#!/bin/bash" >> Run-KMC.sh
	echo " " >> Run-KMC.sh
	echo " " >> Run-KMC.sh

	for t in 2000 2500 3000 3500 4000 4500 5000 10000 20000 30000 40000 50000;do
		if [ $dir == 'X' ];then
			FIELDX=$((1*10**6))
			FIELDY=0
		        FIELDZ=0
		        test -r $t || mkdir $t
		        cp ../options.xml ../state.sql $t
		        sed -i "s/FIELDX/$FIELDX/" $t/options.xml
		        sed -i "s/FIELDY/$FIELDY/" $t/options.xml
		        sed -i "s/FIELDZ/$FIELDZ/" $t/options.xml
		        sed -i "s/TEMP/$t/" $t/options.xml
		        echo "cd $t" >> Run-KMC.sh
		        echo "kmc_run -o options.xml -f state.sql -e kmcmultiple > Out.KMC &" >> Run-KMC.sh
		        echo "cd ../" >> Run-KMC.sh
                        echo " " >> Run-KMC.sh
		elif [ $dir == 'Y' ];then
			FIELDX=0
			FIELDY=$((1*10**6))
		        FIELDZ=0
		        test -r $t || mkdir $t
		        cp ../options.xml ../state.sql $t
		        sed -i "s/FIELDX/$FIELDX/" $t/options.xml
		        sed -i "s/FIELDY/$FIELDY/" $t/options.xml
		        sed -i "s/FIELDZ/$FIELDZ/" $t/options.xml
		        sed -i "s/TEMP/$t/" $t/options.xml
		        echo "cd $t" >> Run-KMC.sh
		        echo "kmc_run -o options.xml -f state.sql -e kmcmultiple > Out.KMC &" >> Run-KMC.sh
		        echo "cd ../" >> Run-KMC.sh
                        echo " " >> Run-KMC.sh
		elif [ $dir == 'Z' ];then
			FIELDX=0
			FIELDY=0
		        FIELDZ=$((1*10**6))
		        test -r $t || mkdir $t
		        cp ../options.xml ../state.sql $t
		        sed -i "s/FIELDX/$FIELDX/" $t/options.xml
		        sed -i "s/FIELDY/$FIELDY/" $t/options.xml
		        sed -i "s/FIELDZ/$FIELDZ/" $t/options.xml
		        sed -i "s/TEMP/$t/" $t/options.xml
		        echo "cd $t" >> Run-KMC.sh
		        echo "kmc_run -o options.xml -f state.sql -e kmcmultiple > Out.KMC &" >> Run-KMC.sh
		        echo "cd ../" >> Run-KMC.sh
                        echo " " >> Run-KMC.sh
		fi
	done
	echo "wait" >> Run-KMC.sh
        chmod +x Run-KMC.sh
        #qsub sub.sh
	cd ..
        done
done
